---
title: Introduction à Android
subtitle: Rapport du laboratoire 1
date: 10 Oct 2021
author:
- Roosembert Palacios
- Joan Maillard
lang: fr
---

## 2.1 Langue de l'interface

Android supporte l'internationalisation des applications par moyen d'un fichier
ressources qui est sélectionné à la volée lors de l'exécution de l'application.

Nous avons créé un fichier XML pour ajouter les traductions des messages en
français. Le fichier correspondant à l'anglais avait déjà été crée par Android
studio.

## 2.2 Champs de texte

Nous avons utilisé un [Pattern Android](https://developer.android.com/reference/android/util/Patterns?hl=en) qui n'est qu'une regex pour valider l'entrée email.

Nous avons également changé le type d'entrée du _widget `TextView`_ à
`textEmailAddress` et `textPassword` afin de prévenir au système d'entrée le
type de donnée attendu ainsi que fournir un rendu plus adéquat (e.g. Le champ
« mot de passe » est caché avec des `*`).

## 2.3 Mode paysage

Nous avons écrit une 'variante' de l'activité principale qui agence les éléments
à afficher d'une manière plus adéquat en mode paysage. Cette variante contient
des widgets avec les mêmes ids que la variante portrait.

## 3 Gestions d'événements et mise à jour de l'interface utilisateur

La gestion d'événements provenant des widgets se fait de manière très similaire
à _java awt_, nous avons créé des objets « listeners » (_desugared from
functionnal interfaces_) au travers de lambdas exécutées lorsque l'événement se
produit.

## 4.1 Création et lancement de la nouvelle activité

La nouvelle activité n'est pas très différente de la première, elle utilise un
nouveau fichier layout (nous n'avons pas créé de variante paysage cette
fois-ci).

## 4.2 Passage de paramètres à la nouvelle activité

Comme nous avons vu en cours, nous avons utilisé les `intents` pour envoyer des
informations de type clé-valeur entre activités.

## 4.3 Permissions simples

Le framework Android exige de déclarer certaines permissions qui peuvent
entraîner une consommation de ressources plus élevée (ou des ressources
« rares », tels que l'accès internet).

De ce fait, nous avons ajouté dans le _manifest_ de l'application le fait
que l'application a besoin d'accès internet.

## 5.1 Création et lancement de la nouvelle activité

Nous avons réutilisé les connaissances déjà apprises au point 4.1 pour la
création de la nouvelle activité.
Par simplicité, nous avons décidé d'utiliser l'ancienne méthode Android.

L'ancienne méthode met en évidence les origines d'Android, où les ressources
étaient très restreintes et potentiellement, le code qui appelle une activité
viendrait à être déchargé du système.
Cette méthode favorise l'utilisation de _méthodes de classes_ qui ne sont pas
au courant du contexte de traitement qui aurait pu invoquer l'activité, mais
seulement de sa valeur de retour.

La nouvelle méthode a une approche plus fonctionnelle en utilisant un objet qui
va servir à gérer le résultat de l'activité. Cet objet peut inclure dans sa
_closure_ des informations (ou variables) dans le contexte qui appelle
l'activité afin de conserver la localité de l'information (d'une manière
similaire à l'appel de fonction) sans avoir à préserver de l'état attendu dans
les champ de la classe (_leak the state_).

## 5.2 Affichage d'une image

Nous avons inclus le fichier dans le dossier `res/drawable` des ressources de
l'application.
Nous aurions pu utiliser n'importe quel endroit dans les ressources de
l'application, mais suivre les bonnes pratiques Android nous permet d'utiliser
certaines fonctions et bénéficier des optimisation sur le paquet (e.g. utilise
un algorithme de compression plus adapté pour les fichiers binaires ou
lempel-ziv pour les fichiers 'texte', ou aussi ne pas compresser les icônes de l'application).

## 5.3 Factorisation du code

Nous avons factorisé la validation à l'aide d'une classe `UserPasswordValidator`
qui réalise les validations communes à l'aide d'une seule fonction.

Nous aurions pu factoriser les interfaces à l'aide des fragments et ainsi créer
une classe qui fait la validation du fragment (en passant également une lambda
pour les vérifications spécifiques à l'activité de _login_ ou création de
compte).

Ces fragments auraient pu être inclus dans les fichiers _layout_ des deux
activités (notamment dans toutes ses variantes) et ainsi créer des nouvelles
variantes avec beaucoup plus de facilité.

## 5.4 Cycle de vie d'une application

Nous avons ajouté à chacune des activités présentes des entrées _log_ lors des événements
principaux relatifs au cycle de vie de l'application. Pour ce faire, nous avons pris
exemple sur l'entrée log déjà présente dans le handout.

Les méthodes que nous avons ajoutées sont appelées principalement lors du changement entre
les activités de l'application. Ainsi, lorsqu'une activité est créée, un appel à _onCreate_
de l'activité concernée est fait, puis un appel à _onPause_ de l'activité dont on vient de sortir,
puis in appel à _onStart_ de l'activité suivante, par exemple (selon le cours, voir image du
lifecycle d'une application).
