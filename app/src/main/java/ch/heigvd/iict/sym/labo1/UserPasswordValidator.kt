package ch.heigvd.iict.sym.labo1

import android.content.Context
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast

class UserPasswordValidator(
    private val emailField: EditText,
    private val passwordField: EditText,
    private val context: Context,
) {
    fun validate(): Boolean {
        // Reset field errors.
        emailField.error = null
        passwordField.error = null

        val emailInput = emailField.text?.toString()
        val passwordInput = passwordField.text?.toString()

        if (emailInput.isNullOrEmpty() or passwordInput.isNullOrEmpty()) {
            Log.d(TAG, "Either the email or password are not valid.")
            // Set the adequate field errors.
            if (emailInput.isNullOrEmpty())
                emailField.error = context.getString(R.string.main_mandatory_field)
            if (passwordInput.isNullOrEmpty())
                passwordField.error = context.getString(R.string.main_mandatory_field)
            return false
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailInput.toString()).matches()) {
            Log.d(TAG, "Email can't be matched to expected expression")
            emailField.error = context.getString(R.string.invalid_email)

            val text = context.getString(R.string.invalid_email)
            val duration = Toast.LENGTH_SHORT
            Toast.makeText(context, text, duration).show()
            return false
        }

        return true
    }

    companion object {
        const val TAG: String = "UserPasswordValidator"
    }
}