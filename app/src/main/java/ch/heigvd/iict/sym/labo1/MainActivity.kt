package ch.heigvd.iict.sym.labo1

import android.accounts.Account
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.util.Patterns
import android.app.AlertDialog
import android.content.Intent
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    // on définit une liste de couples e-mail / mot de passe
    // ceci est fait juste pour simplifier ce premier laboratoire,
    // mais il est évident que de hardcoder ceux-ci est une pratique à éviter à tout prix...
    // /!\ listOf() retourne une List<T> qui est immuable
    private val credentials = mutableListOf(
                                Pair("user1@heig-vd.ch","1234"),
                                Pair("user2@heig-vd.ch","abcd")
                            )

    // le modifieur lateinit permet de définir des variables avec un type non-null
    // sans pour autant les initialiser immédiatement
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var cancelButton: Button
    private lateinit var validateButton: Button
    private lateinit var newAccount: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        // l'appel à la méthode onCreate de la super classe est obligatoire
        super.onCreate(savedInstanceState)
        // on définit le layout à utiliser pour l'affichage
        setContentView(R.layout.activity_main)

        emailField = findViewById(R.id.main_email)
        passwordField = findViewById(R.id.main_password)
        Log.d(TAG, "MainActivity OnCreate")
        cancelButton = findViewById(R.id.main_cancel)
        validateButton = findViewById(R.id.main_validate)
        newAccount = findViewById(R.id.main_new_account)
        // Kotlin, au travers des Android Kotlin Extensions permet d'automatiser encore plus cette
        // étape en créant automatiquement les variables pour tous les éléments graphiques présents
        // dans le layout et disposant d'un id
        // cf. https://medium.com/@temidjoy/findviewbyid-vs-android-kotlin-extensions-7db3c6cc1d0a

        //mise en place des événements
        cancelButton.setOnClickListener {
            //on va vider les champs de la page de login lors du clique sur le bouton Cancel
            emailField.text?.clear()
            passwordField.text?.clear()
            // on annule les éventuels messages d'erreur présents sur les champs de saisie
            emailField.error = null
            passwordField.error = null
        }

        val formValidator = UserPasswordValidator(emailField, passwordField, this)

        validateButton.setOnClickListener {
            val emailInput = emailField.text?.toString()
            val passwordInput = passwordField.text?.toString()

            if (!formValidator.validate()) {
                return@setOnClickListener;
            }

            val cred = Pair(emailInput, passwordInput)
            if (cred in credentials) {
                val intent = Intent(this, DisplayUserActivity::class.java).apply {
                    putExtra("ch.heigvd.iict.sym.LOGIN_EMAIL", emailInput)
                }
                startActivity(intent)
            } else {
                Log.d(TAG, "Invalid credentials")
                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.invalid_credentials))
                builder.setMessage(getString(R.string.invalid_credential_verbose))
                val alertDialog: AlertDialog = builder.create()
                alertDialog.setCancelable(true)
                alertDialog.show()
                return@setOnClickListener
            }
        }

        newAccount.setOnClickListener {
            val intent = Intent(this, AccountCreationActivity::class.java)
            startActivityForResult(intent, REQUEST_RESULT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d(TAG, "MainActivity OnActivityResult")

        if (requestCode == REQUEST_RESULT) {
            if(resultCode == Activity.RESULT_OK && data != null) {
                val usr = data.getStringExtra(AccountCreationActivity.RESULT_USR)
                val pwd = data.getStringExtra(AccountCreationActivity.RESULT_PWD)
                val cred = Pair(usr.toString(), pwd.toString())
                credentials.add(cred)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "MainActivity OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "MainActivity OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "MainActivity OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "MainActivity OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "MainActivity OnRestart")
    }

    // En Kotlin, les variables static ne sont pas tout à fait comme en Java
    // pour des raison de lisibilité du code, les variables et méthodes static
    // d'une classe doivent être regroupées dans un bloc à part: le companion object
    // cela permet d'avoir un aperçu plus rapide de tous les éléments static d'une classe
    // sans devoir trouver le modifieur dans la définition de ceux-ci, qui peuvent être mélangé
    // avec les autres éléments non-static de la classe
    companion object {
        private const val REQUEST_RESULT = 1
        private const val TAG: String = "MainActivity"
    }

}
