package ch.heigvd.iict.sym.labo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import ch.heigvd.iict.sym.labo1.network.ImageDownloader

class DisplayUserActivity : AppCompatActivity() {

    private lateinit var usedEmailTextView: TextView
    private lateinit var internetImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_user)

        Log.d(TAG, "DisplayUserActivity OnCreate")

        val usedEmail = intent.getStringExtra("ch.heigvd.iict.sym.LOGIN_EMAIL")
        usedEmailTextView = findViewById(R.id.emailTextView)
        usedEmailTextView.text = usedEmail

        internetImageView = findViewById(R.id.imageView)
        ImageDownloader(internetImageView, "https://thispersondoesnotexist.com/image").show()

    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "DisplayUserActivity OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "DisplayUserActivity OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "DisplayUserActivity OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "DisplayUserActivity OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "DisplayUserActivity OnRestart")
    }

    companion object {
        private const val TAG: String = "DisplayUserActivity"
    }
}