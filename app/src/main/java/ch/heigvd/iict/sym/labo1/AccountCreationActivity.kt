package ch.heigvd.iict.sym.labo1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText

class AccountCreationActivity : AppCompatActivity() {
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var submitButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_creation)

        Log.d(TAG, "AccountCreationActivity OnCreate")

        emailField = findViewById(R.id.emailInput)
        passwordField = findViewById(R.id.passwordInput)
        submitButton = findViewById(R.id.submitButton)

        val formValidator = UserPasswordValidator(emailField, passwordField, this)

        submitButton.setOnClickListener {
            if (formValidator.validate()) {
                val intent = Intent().apply {
                    putExtra(RESULT_USR, emailField.text?.toString())
                    putExtra(RESULT_PWD, passwordField.text?.toString())
                }
                setResult(RESULT_OK, intent)
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "AccountCreationActivity OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "AccountCreationActivity OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "AccountCreationActivity OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "AccountCreationActivity OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "AccountCreationActivity OnRestart")
    }

    companion object {
        const val RESULT_USR = "ch.heigvd.iict.sym.labo1.NEW_USER"
        const val RESULT_PWD = "ch.heigvd.iict.sym.labo1.NEW_PWD"
    }
}
